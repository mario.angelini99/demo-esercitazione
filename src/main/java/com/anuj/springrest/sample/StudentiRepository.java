package com.anuj.springrest.sample;

import org.springframework.data.repository.CrudRepository;

public interface StudentiRepository extends CrudRepository<Studenti, String> {
}
