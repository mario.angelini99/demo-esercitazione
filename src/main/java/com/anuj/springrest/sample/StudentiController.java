package com.anuj.springrest.sample;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/demo")
public class StudentiController {

	@Autowired
	private StudentiService studentiService;

	@GetMapping(path = "/all")
	public List<Studenti> getAllUsers() {
		return studentiService.printAll();
	}

	@GetMapping("/user/{matricola}")
	public Studenti getUserId(@PathVariable String matricola) {
		return studentiService.getUserById(matricola);
	}

	@PostMapping("/add")
	public void saveStudent(@RequestBody Studenti studente) {
		studentiService.insertStudent(studente);
	}

	@PutMapping(path = "/user/{matricola}")
	public String update(@RequestBody Studenti studente, @PathVariable String matricola) {
		return studentiService.updateStudentById(matricola, studente);
	}

	@PutMapping(path = "/user")
	public String updateById(@RequestBody Studenti studente) {
		return studentiService.updateStudent(studente);
	}

	@DeleteMapping("/user/{matricola}")
	public String delete(@PathVariable String matricola) {
		return studentiService.deleteStudent(matricola);
	}

}
