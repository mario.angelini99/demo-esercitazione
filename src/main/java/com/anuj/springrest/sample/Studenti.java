package com.anuj.springrest.sample;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "Studenti")
@NamedQuery(name = "Studenti.findAll", query = "SELECT s FROM Studenti s")
@Getter
@Setter
public class Studenti implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "matricola")
	private String matricola;

	@Column(name = "age")
	private int age;

	@Column(name = "cognome")
	private String cognome;

	@Column(name = "nome")
	private String nome;

	@Override
	public String toString() {
		return "Studente{" + "matricola=" + matricola + ", nome='" + nome + '\'' + ", cognome='" + cognome + '\''
				+ ", età='" + age + '\'' + '}';
	}

	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof Studenti))
			return false;

		Studenti ob = (Studenti) o;

		return matricola.equals(ob.matricola);
	}

	@Override
	public int hashCode() {
		return matricola.hashCode();
	}

}