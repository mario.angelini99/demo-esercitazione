package com.anuj.springrest.sample;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StudentiDTO {

	@JsonProperty("age")
	private int age;

	@JsonProperty("cognome")
	private String cognome;

	@JsonProperty("matricola")
	private String matricola;

	@JsonProperty("nome")
	private String nome;

}
