package com.anuj.springrest.sample;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

@Service
public class StudentiService {
	@Autowired
	private StudentiRepository studentiRepository;

	List<Studenti> arrayListStudenti = new ArrayList<Studenti>();

	// Get tutti gli studenti
	public List<Studenti> printAll() {
		arrayListStudenti.clear();
		studentiRepository.findAll().forEach(arrayListStudenti::add);
		return arrayListStudenti;
	}

	// Get uno studente con id inserito nell'url
	public Studenti getUserById(String matricola) {
		arrayListStudenti.clear();
		studentiRepository.findById(matricola).ifPresent(arrayListStudenti::add);
		return arrayListStudenti.get(0);
	}

	// Post - inserimento di uno studente
	public Studenti insertStudent(Studenti studente) {
		studentiRepository.save(studente);
		return studente;
	}

	// Put - update di uno studente
	public String updateStudent(Studenti studente) {
		if (studente.getMatricola() != null) {
			Optional<Studenti> fetchedStud = studentiRepository.findById(studente.getMatricola());
			if (fetchedStud.isPresent()) {
				Studenti stud = fetchedStud.get();
				stud.setNome(studente.getNome());
				stud.setCognome(studente.getCognome());
				stud.setAge(studente.getAge());
				studentiRepository.save(studente);
				return "User Updated for user id: " + studente.getMatricola();
			}
		}
		return "User Id is not VALID";
	}

	// Put - update di uno studente by id
	public String updateStudentById(String matricola, Studenti studente) {
		if (matricola != null) {
			Optional<Studenti> FindStud = studentiRepository.findById(studente.getMatricola());
			if (FindStud.isPresent()) {
				Studenti stud = FindStud.get();
				stud.setNome(studente.getNome());
				stud.setCognome(studente.getCognome());
				stud.setAge(studente.getAge());
				studentiRepository.save(studente);
				return "User Updated for user id: " + studente.getMatricola();
			}
		}
		return "User Id is not VALID";
	}

	// Delete - cancella uno studente passando id nell'url
	public String deleteStudent(@PathVariable String matricola) {
		studentiRepository.deleteById(matricola);
		return "Deleted with customer id: " + matricola;
	}

}
